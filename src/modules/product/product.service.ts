import { Op } from "sequelize";
import { DummyProduct, Filters, Product } from "../../types/product";
import { ProductModel } from "./product.model";
import fetch from 'node-fetch';

export async function createProduct(product: Omit<Product, "id">) {
  return ProductModel.create(product);
}

export async function updateProduct(id: number, product: Omit<Product, "id">) {
  return ProductModel.update({ ...product }, {
    where: {
      id
    }
  });
}

export async function deleteProduct(id: number) {
  return ProductModel.destroy({
    where: {
      id
    }
  });
}

export async function getProduct(id: number) {
  return ProductModel.findOne({
    where: {
      id
    }
  });
}

export async function getFilteredProducts(filters: Filters) {
  const whereStatement = {
    [Op.or]: [
      { title: { [Op.like]: '%' + filters.search + '%' } },
      { category: { [Op.like]: '%' + filters.search + '%' } },
      { brand: { [Op.like]: '%' + filters.search + '%' } },
    ],
    price: { [Op.between]: [filters.price[0], filters.price[1]] },
    discountPercentage: { [Op.between]: [filters.discount[0], filters.discount[1]] },
  }

  if (Number(filters.rating)) {
    Object.assign(whereStatement, {
      rating: {
        [Op.and]: [{ [Op.gt]: filters.rating - 1 }, { [Op.lte]: filters.rating }]
      },
    });
  }

  return ProductModel.findAndCountAll({
    where: whereStatement,
    limit: filters.limit,
    offset: filters.offset,
  });
}

export async function saveProductsFromDummy() {
  const response = await fetch('https://dummyjson.com/products/');
  const data: { products: DummyProduct[] } = await response.json();
  const products = data.products.map(product => {
    return {
      title: product.title,
      description: product.description,
      price: product.price,
      discountPercentage: product.discountPercentage,
      rating: product.rating,
      stock: product.stock,
      brand: product.brand,
      category: product.category,
      thumbnail: product.thumbnail,
      images: product.images,
    }
  });

  return ProductModel.bulkCreate(products);
}

export async function getProducts() {
  return ProductModel.findAll()
}