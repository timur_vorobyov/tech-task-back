import { Filters, Product } from '../../types/product';
import {
  createProduct,
  deleteProduct,
  getProduct,
  getFilteredProducts,
  saveProductsFromDummy,
  updateProduct,
  getProducts
} from './product.service';
import { Request, Response } from 'express-serve-static-core';
import { StatusCodes } from "http-status-codes";

export async function createProductHandler(
  req: Request<Record<string, never>, Record<string, never>, Omit<Product, "id">>,
  res: Response) {
  try {
    await createProduct(req.body);

    return res.status(StatusCodes.CREATED).send("PRODUCT_CREATED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function updateProductHandler(
  req: Request<{ id: number }, Record<string, never>, Omit<Product, "id">>,
  res: Response) {
  try {
    await updateProduct(req.params.id, req.body);

    return res.status(StatusCodes.OK).send("PRODUCT_UPDATED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function deleteProductHandler(
  req: Request<{ id: number }, Record<string, never>, Record<string, never>>,
  res: Response) {
  try {
    await deleteProduct(req.params.id);

    return res.status(StatusCodes.OK).send("PRODUCT_REMOVED_SUCCESSFULY");
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function getProductHandler(
  req: Request<{ id: number }, Record<string, never>, Record<string, never>>,
  res: Response) {
  try {
    const product = await getProduct(req.params.id);

    return res.status(StatusCodes.OK).send(product);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function getFilteredProductsHandler(
  req: Request<Record<string, never>, Record<string, never>, Record<string, never>, Filters>,
  res: Response) {
  try {
    const { count, rows } = await getFilteredProducts(req.query);

    return res.status(StatusCodes.OK).send({
      count,
      products: rows
    });
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function saveProductsFromDummyHandler(
  req: Request<Record<string, never>, Record<string, never>, Record<string, never>>,
  res: Response) {
  try {
    await saveProductsFromDummy();

    return res.status(StatusCodes.OK).send();
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function getProductsHandler(
  req: Request<Record<string, never>, Record<string, never>, Record<string, never>>,
  res: Response) {
  try {
    const products = await getProducts();

    return res.status(StatusCodes.OK).send(products);
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}

export async function uploadProductImagesHandler(
  req: Request<Record<string, never>, Record<string, never>, Record<string, never>>,
  res: Response) {
  try {
    const files = { ...req.files } as { thumbnail: Express.Multer.File[], images: Express.Multer.File[] }
    const thumbnail = files.thumbnail[0].filename;
    const images = files.images.map(image => image.filename)

    return res.status(StatusCodes.OK).send({
      thumbnail, images
    });
  } catch (e) {
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e);
  }
}
