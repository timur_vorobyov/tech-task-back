import express from "express";
import multer from "multer";
import { v4 } from 'uuid';
import {
  createProductHandler,
  deleteProductHandler,
  saveProductsFromDummyHandler,
  getProductHandler,
  getFilteredProductsHandler,
  updateProductHandler,
  getProductsHandler,
  uploadProductImagesHandler,
} from "./product.controller";
import { Request } from 'express-serve-static-core';
import path from 'path'

type DestinationCallback = (error: Error | null, destination: string) => void
type FileNameCallback = (error: Error | null, filename: string) => void

const router = express.Router();


const storage = multer.diskStorage({
  destination: function (req: Request, file: Express.Multer.File, cb: DestinationCallback) {
    cb(null, './uploads')
  },
  filename: function (
    req: Request,
    file: Express.Multer.File,
    cb: FileNameCallback
  ) {
    cb(null, file.fieldname + '-' + v4() + path.extname(file.originalname))
  }
});
const upload = multer({ storage: storage });


router.post(
  '/upload',
  upload.fields([{ name: 'thumbnail', maxCount: 1 }, { name: 'images', maxCount: 8 }]),
  uploadProductImagesHandler
);
router.get('/filter', getFilteredProductsHandler);
router.post('/download', saveProductsFromDummyHandler);
router.post('/', createProductHandler);
router.put('/:id', updateProductHandler);
router.delete('/:id', deleteProductHandler);
router.get('/:id', getProductHandler);
router.get('/', getProductsHandler);

export default router;