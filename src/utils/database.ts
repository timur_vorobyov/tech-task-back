import config from '../config';
import { Sequelize } from 'sequelize';



const sequelize = new Sequelize(
  config.database,
  config.userName,
  config.password,
  {
    dialect: 'postgres',
    host: config.hostName,
  }
);

export default sequelize