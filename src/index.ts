import cors from 'cors';
import 'dotenv/config';
import express from 'express';
import config from './config';
import productRoute from './modules/product/product.route';
import sequelize from './utils/database';
import logger from './utils/logger';
import path from 'path'

const app = express();

app.use(express.json());
app.use(express.static(path.resolve(__dirname, '..', 'uploads')))
app.use(cors({
  origin: config.corsOrigin,
  credentials: true
}));
app.use('/api/products', productRoute);

(async () => {
  try {
    sequelize.sync({
      force: false
    })

    app.listen(config.port, () => {
      logger.info(`Server listening at http://localhost:${config.port}`);
    })
  } catch (e) {
    logger.error(e, "Failed to connect to databae, Goodbye");
    process.exit(1);
  }
})()


