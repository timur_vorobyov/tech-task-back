const config = {
  port: process.env.PORT || "4000",
  hostName: process.env.HOST || "node_db",
  userName: process.env.USER || "timur",
  password: process.env.PASSWORD || "12345",
  database: process.env.DB || "tabnine_db",
  corsOrigin: process.env.CORS_ORIGIN || "http://localhost:3000"
}

export default config