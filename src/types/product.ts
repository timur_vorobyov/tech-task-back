export interface Product {
  id: number,
  title: string,
  description: string,
  price: number,
  discountPercentage: number,
  rating: number,
  stock: number,
  brand: string,
  category: string,
  thumbnail: string,
  images: string[]
}

export interface Filters {
  limit: number,
  offset: number,
  search: string,
  rating: number,
  price: number[],
  discount: number[]
}

export type DummyProduct = Product