FROM node:14.19.0-slim
EXPOSE 3001
WORKDIR /tech-task
RUN mkdir uploads
COPY package.json /tech-task
RUN npm install
COPY . /tech-task
CMD ["npm", "run", "dev"]